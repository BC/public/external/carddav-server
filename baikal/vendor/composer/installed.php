<?php return array(
    'root' => array(
        'name' => 'fruux/baikal',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '39882384483ef5a749ce403d9d167a5849d294ed',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'fruux/baikal' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '39882384483ef5a749ce403d9d167a5849d294ed',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jeromeschneider/baikal' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'dev-master',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/dav' => array(
            'pretty_version' => '4.3.1',
            'version' => '4.3.1.0',
            'reference' => '130abb7017f56e0d99b04eb94b041e000a8e9b39',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/dav',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/event' => array(
            'pretty_version' => '5.1.4',
            'version' => '5.1.4.0',
            'reference' => 'd7da22897125d34d7eddf7977758191c06a74497',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/event',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/http' => array(
            'pretty_version' => '5.1.6',
            'version' => '5.1.6.0',
            'reference' => '9976ac34ced206bd6579b7b37b401de9fac98dae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/http',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/uri' => array(
            'pretty_version' => '2.3.2',
            'version' => '2.3.2.0',
            'reference' => 'eceb4a1b8b680b45e215574222d6ca00be541970',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/uri',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/vobject' => array(
            'pretty_version' => '4.5.1',
            'version' => '4.5.1.0',
            'reference' => '1f836740c88bac483f3b572a332eb8fd1cd04981',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/vobject',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabre/xml' => array(
            'pretty_version' => '2.2.5',
            'version' => '2.2.5.0',
            'reference' => 'a6af111850e7536d200d9637c34885cd3c77a86c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabre/xml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => '88289caa3c166321883f67fe5130188ebbb47094',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v2.14.13',
            'version' => '2.14.13.0',
            'reference' => '66856cd0459df3dc97d32077a98454dc2a0ee75a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
